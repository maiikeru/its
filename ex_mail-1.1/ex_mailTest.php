<?php

require "ex_mail.php";

class MyMailTest extends PHPUnit_Framework_TestCase
{
    public function test1()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';
        
        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test2()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("", "Subject text", "False č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test3()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test4()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("", "Subject text", "False č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }


    public function test5()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test6()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test8()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test7()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test9()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test10()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("", "Subject text", "False č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test11()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test12()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("", "Subject text", "False č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test13()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test14()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test15()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test16()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test17()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test18()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 1, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test19()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test20()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 5, "A");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test21()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test22()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test23()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test24()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test25()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test26()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test27()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test28()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "False č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test29()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test30()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 1, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }

    public function test31()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "Objednávka č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    public function test32()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("", "Subject text", "Objednávka č. 1 2", "body", 5, "");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }
}
?>

