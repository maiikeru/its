<?php
/* Pripraveno pro test doubles. */

/* Primo vyzadovano testovanym souborem. */

class Mail_mime
{

    private $body;
    private $headers = array();

    function __construct()
    {
        $this->body = "";
    }

    public function setHTMLBody($body)
    {
        $this->body = $body;
    }

    public function get()
    {
        return $this->body;
    }

    public function headers($headers)
    {
        $this->headers = $headers;
        return $this->headers;
    }
}

?>
