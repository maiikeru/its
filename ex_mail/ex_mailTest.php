<?php

require "ex_mail.php";

class MyMailTest extends PHPUnit_Framework_TestCase
{
    public function testSingleMailSent()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "Nadpis x y", "body");

        $this->assertEquals(SmtpObject::$body, "body");
    }

    //0,2,3,4,5,7,8
    public function testSingleMailSent1()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("to@example.com", "Subject text", "Nadpis x y", "body");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, true);
    }

    //0,2,3,4,5,7,9
    public function testSingleMailSent2()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '7';

        MyMail::pear_mail("", "Subject text", "Nadpis x y", "body");

        $this->assertEquals(DB::$prof, "7");
        $this->assertEquals(PEAR::$state, false);
    }

    //0,1,2,3,4,5,7,8
    public function testSingleMailSent3()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("to@example.com", "Subject text", "Nadpis x y", "body");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, true);
    }

    //0,1,2,3,4,5,7,8
    public function testSingleMailSent4()
    {
        global $CONFIG, $ERRORMSG;

        $CONFIG['mail_odesilatel'] = 'from@example.com';
        $CONFIG['mail_server'] = 'smtp.example.com';
        $CONFIG['mail_server_name'] = 'smtp.example.com';
        $CONFIG['mail_server_pass'] = 's3cr3dp4ssw0rd';
        $CONFIG['mail_server_port'] = '25';
        $CONFIG['proforma_splatnost'] = '1';

        MyMail::pear_mail("", "Subject text", "Nadpis x y", "body");

        $this->assertEquals(DB::$prof, "1");
        $this->assertEquals(PEAR::$state, false);
    }
}

?>

