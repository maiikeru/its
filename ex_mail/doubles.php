<?php
/* Pripraveno pro test doubles. */

/* Vyzadovano bootstrap.php jako vychozi zavislosti. */

class DB
{
    public static $prof;

    public static function DbQuery($string)
    {
        global $CONFIG;


        self::$prof = $CONFIG['proforma_splatnost'];

        $neo = array('proforma' => self::$prof);
        return $neo;
    }
}

class PEAR
{
    public static $state = false;

    public static function isError(Crap $mail)
    {
        self::$state = $mail->getState() ? false : true;
        return self::$state;
    }
}

/**
 * @property  body
 */
class SmtpObject
{
    public static $body;
    private static $smtp;
    private static $array;

    function __construct($string, $array)
    {
        self::$smtp = $string;
        self::$array = $array;
    }

    public static function send($to, $headers, $body)
    {
        self::$body = $body;

        if (strcmp(self::$smtp, "smtp") != 0)
            return new Crap(false);

        if (strcmp($to, "") != 0)
            return new Crap(false);

        return new Crap(true);
    }
}

class Crap
{

    private $state;

    function __construct($bool)
    {
        $this->state = $bool;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getMessage()
    {
        return "Mail not send, SMTP is missing. ";
    }
}


?>
